<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Pagina de Inicio</title>
    </head>
    <body>
        <h1>Esta es la página de inicio de la aplicación</h1>
        <br/>A continuación algunos ejemplos:<br/>
        <br/><a href="cabeceras.html">Ejemplo Cabeceras</a>
        <br/><a href="autos.html">Ejemplo Cabeceras con Imagen</a>
        <br/><a href="tablas.html">Ejemplo Tablas</a>
        <br/><a href="formulario.html">Ejemplo Formulario</a>
    </body>
</html>
